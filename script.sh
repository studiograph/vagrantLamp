#!/bin/bash

# Updating repository

sudo apt-get -y update

# Install Apache

sudo apt-get -y install apache2

# Installing MySQL and it's dependencies, Also, setting up root password for MySQL as it will prompt to enter the password during installation

sudo debconf-set-selections <<< 'mysql-server-5.5 mysql-server/root_password password vagrant'
sudo debconf-set-selections <<< 'mysql-server-5.5 mysql-server/root_password_again password vagrant'
sudo apt-get -y install mysql-server libapache2-mod-auth-mysql php5-mysql

# Installing PHP and it's dependencies
sudo apt-get -y install php5 libapache2-mod-php5 php5-mcrypt

sudo /etc/init.d/mysql stop
sudo /usr/sbin/mysqld --skip-grant-tables --skip-networking &
# Wait for mysql to start up
until mysqladmin ping; do
	sleep 2
done
echo "lalalalalal"
#sed -i -e 's/\/var//g' "/etc/mysql/my.cnf"
#sed -i -e 's/\/var//g' "/etc/mysql/debian.cnf"