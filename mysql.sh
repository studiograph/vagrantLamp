#!/bin/sh

# Set root password to 'pass'
# Create vagrant user with no pass for ease of use during ssh
`mysql -u root -pvagrant -e "\
FLUSH PRIVILEGES; \
SET PASSWORD FOR root@'localhost' = PASSWORD('vagrant'); \
FLUSH PRIVILEGES; \
\
GRANT ALL PRIVILEGES ON *.* TO 'vagrant'@'localhost' \
WITH GRANT OPTION; \
FLUSH PRIVILEGES; "`

echo "ha"

if [ ! -z `ls /vagrant/*.sql` ]; then	
	# Set variable to the basename of the file, minus '.sql'
	DBNAME=`ls /vagrant/*.sql | cut -d '/' -f 3 | sed s/.sql//`
	
	# Create a database with that name
	mysqladmin -u root -ppass create $DBNAME
	
	# Import the SQL into new database
	`mysql -u root -pvagrant $DBNAME < /vagrant/$DBNAME.sql`
	
	# Create a new user with same name as new db, with password 'pass'
	`mysql -u root -pvagrant -e "\
		GRANT ALL PRIVILEGES ON $DBNAME.* TO '$DBNAME'@'%' \
		IDENTIFIED BY 'pass' WITH GRANT OPTION; \
		FLUSH PRIVILEGES; "`
fi

echo "Allow remote connections to mysql (don't do it like this in production)"
sudo sed -i 's/bind-address		= 127.0.0.1/bind-address		= 0.0.0.0/' /etc/mysql/my.cnf

echo "kill mysql"
sudo mysqladmin -u root -pvagrant shutdown
sudo service mysql stop
echo "next error is fine, we shut down mysql"
while mysqladmin -u root -pvagrant ping; do
	sleep 2
done
sleep 2
echo "startup mysql again"
sudo service mysql start
echo "put adminer in it's place"
cp /vagrant/adminer.php /var/www/html/adminer.php
cp /vagrant/adminer.css /var/www/html/adminer.css
echo "DOOONE"